@extends('layouts.app')

@section('content')
    <div clas="container">
        <ul>
            @forelse($notifications as $notification)
                <li>
                    @if($notification->type == 'App\Notifications\PaymentReceived')
                        We have received a payment of {{ $notification->data['amount'] }} from you.
                    @endif
                </li>
            @empty
                <li>You have no notifications</li>
            @endforelse
        </ul>
    </div>

@endsection
